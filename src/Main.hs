module Main where

import Brick
import Brick.AttrMap
import Control.Monad (void)
import Graphics.Vty.Input.Events
import Graphics.Vty.Attributes
import qualified Data.Map as M

data CoreState = MkCoreState
    { csBuffers :: M.Map FilePath String -- TODO: rope
    }

defaultCoreState :: CoreState
defaultCoreState = MkCoreState
    { csBuffers = M.empty
    }

coreApp :: App CoreState Event
coreApp = App
    { appDraw         = \s -> [helloLabel]
    , appChooseCursor = \s crs -> Nothing
    , appStartEvent   = \s -> return s
    , appHandleEvent  = coreHandleEvent
    , appAttrMap      = \s -> attrMap (Attr KeepCurrent KeepCurrent KeepCurrent) []
    , appLiftVtyEvent = \e -> e
    }

helloLabel :: Widget
helloLabel = str "Hello World!"

coreHandleEvent :: CoreState -> Event -> EventM (Next CoreState)
coreHandleEvent s (EvKey KEsc _) = halt s
coreHandleEvent s (EvResize _ _) = continue s
coreHandleEvent s _ = continue s

main :: IO ()
main = void $ defaultMain coreApp defaultCoreState
